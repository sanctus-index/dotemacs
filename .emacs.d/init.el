;;; init.el --- initialisation file for `config.org'

;; Copyright (C) 2020-2021 Finn Sauer <info@finnsauer.com>
;;
;; This file is part of my `config.org' ecosystem.
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or (at
;; your option) any later version.
;;
;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This is the initialisation file for GNU/Emacs.  At the end of this
;; file, it will call the proper configuration file written in
;; `org-mode'.  Visit that document to see the proper / full
;; documentation, code-snippets, and the idea behind it.
;;
;; The sole purpose of this file is to initialise loading the proper
;; configuration file.  Everything else is out-of-scope.
;;
;; Although you will find some code that SHOULD belong in the
;; `config.org', but I have put it here since it is important to be set
;; at the beginning.

;;; Code :

(eval-and-compile
  ;; If `native-comp' is available use `gcc -O3' instead of `gcc -O2'.
  (when (ignore-errors (native-comp-available-p))
    (setq comp-speed 3))

  ;; This is for deferring the check for modifications mechanism to
  ;; `check-on-save' instead of `find-at-startup'-- which as the name
  ;; implies runs a command at startup.  The command is quite slow.
  (setq straight-check-for-modifications '(check-on-save))

  ;; Bootstrap code for `straight.el'.  This has been slightly modified.
  (let ((bootstrap-file (expand-file-name
                         "straight/repos/straight.el/bootstrap.el"
                         user-emacs-directory))
        (bootstrap-version 5))
    (unless (file-exists-p bootstrap-file)
      (with-current-buffer (url-retrieve-synchronously
                            (concat
                             "https://raw.githubusercontent.com/"
                             "raxod502/straight.el/develop/install.el")
                            'silent 'inhibit-cookies)
        (goto-char (point-max))
        (eval-print-last-sexp)))
    (load bootstrap-file nil 'nomessage))
  ;; Bootstrap ends here

  ;; Install various essential packages such as `leaf' and
  ;; `leaf-keywords'.  The `cl-lib' package is already required in
  ;; straight's `bootstrap.el'.  I require it here again for completion
  ;; sake.
  (dolist (p '(leaf leaf-keywords org cl-lib dash s))
    (straight-use-package p)
    (require p))

  ;; Uses a different file for `custom' in order to not clutter the
  ;; `init.el' file with automatic settings made by custom.  It resides
  ;; in a temporary file, that will be newly generated each time Emacs
  ;; is launched---thus disabling it.
  (setq custom-file (make-temp-file "emacs-custom-"))

  ;; Do not ask to follow symlinks that are inside a Git directory.
  ;; Reason being mainly loading the configuration file
  ;; `config.org'---since it is a symlink which resides in a Git
  ;; repository.
  (setq vc-follow-symlinks t)

  ;; This is the actual configuration file.  This will try to load four
  ;; files: `config.eln', `config.elc', `config.el', and `config.org'.
  ;; If none are found throw an error.  Both `config.elc' and
  ;; `config.el'-- and if `native-comp' is available: `config.eln'-- are
  ;; recompiled on exit with `index/rebuild-emacs-init'.
  (let* ((b (expand-file-name "config" user-emacs-directory))
         (o (s-append ".org" b))
         (e (s-append ".el" b))
         (c (s-append ".elc" b))
         (n (s-append ".eln" b)))
    (leaf-keywords-init)
    (cond
     ((file-readable-p n) (load-file n))           ; Load `config.eln'
     ((file-readable-p c) (load-file c))           ; Load `config.elc'
     ((file-readable-p e) (load-file e))           ; Load `config.el'
     ((file-readable-p o) (org-babel-load-file o)) ; Load `config.org'
     (:else (error "Found no init file in `user-emacs-directory'")))))

;;; init.el ends here
